-- References
-- http://www.iriel.org/wow/docs/SecureHeadersGuide-4.0-r1.pdf
-- https://wow.gamepedia.com/Secure_Execution_and_Tainting

-- Local API
local CreateFrame = CreateFrame
local C_Timer = C_Timer
local debugstack = debugstack
local UIParent = UIParent
local GetTime = GetTime
local GetCursorPosition = GetCursorPosition
local GetAddOnMetadata = GetAddOnMetadata
local GameFontHighlightSmall = GameFontHighlightSmall
local InterfaceOptions_AddCategory = InterfaceOptions_AddCategory

-- Addon
-- RENAME TO MAP-PORT?
MagePort = CreateFrame("Frame", "MagePort", UIParent)
local Addon = MagePort

-- Allow 'Esc' key to close frames
tinsert(UISpecialFrames, Addon:GetName())

-- Addon states
Addon.INVISIBLE = 0
Addon.FADE_IN   = 1
Addon.VISIBLE   = 2
Addon.FADE_OUT  = 3

local ZOOM = 1.05
local CRLF = "\r\n"

-- Default values
Addon.state  = Addon.INVISIBLE
Addon:SetAttribute("state", Addon.state)
Addon.size   = 384 * UIParent:GetEffectiveScale()
Addon.speed  = 0.150
Addon.toggle = false
Addon.alpha  = 0

-- Restore values from save file
function Addon:Init()
  Addon.size   = savesChar.size     or Addon.size
  Addon.toggle = savesChar.toggle   or Addon.toggle
  
  Addon.panel.size:SetValue(Addon.size)
  Addon.panel.toggle:SetChecked(Addon.toggle)
end

-- Key binding strings, matched to Bindings.xml at runtime by the wow client
BINDING_HEADER_MAGEPORT = "MagePort"
-- BINDING_NAME_SHOWPORTALMAP = "Show Portal Map"
_G["BINDING_NAME_CLICK Secure:LeftButton"] = "Show Portal Map"

-- Anchor point
Addon:SetPoint("CENTER", UIParent, "CENTER")
Addon:SetWidth(1)
Addon:SetHeight(1)
Addon:SetAlpha(0)
Addon:Hide()

-- Left/Right/Center
Addon.l = CreateFrame("Frame", Addon:GetName() .. "Left",   Addon)
Addon.r = CreateFrame("Frame", Addon:GetName() .. "Right",  Addon)
Addon.c = CreateFrame("Frame", Addon:GetName() .. "Center", Addon)

Addon.frames = {Addon.l, Addon.c, Addon.r}

local initialSize = Addon.size * ZOOM
Addon.l:SetSize(initialSize,initialSize)
Addon.r:SetSize(initialSize,initialSize)
Addon.c:SetSize(initialSize,initialSize)

Addon.c:SetPoint("CENTER", Addon, "CENTER")
Addon.l:SetPoint("RIGHT", Addon.c, "LEFT")
Addon.r:SetPoint("LEFT", Addon.c, "RIGHT")

Addon.l.t = Addon:CreateTexture()
Addon.r.t = Addon:CreateTexture()
Addon.c.t = Addon:CreateTexture()

Addon.l.t:SetTexture("Interface/TAXIFRAME/TAXIMAP1")
Addon.r.t:SetTexture("Interface/TAXIFRAME/TAXIMAP530")
Addon.c.t:SetTexture("Interface/TAXIFRAME/TAXIMAP0")

Addon.l.t:SetAllPoints(Addon.l)
Addon.r.t:SetAllPoints(Addon.r)
Addon.c.t:SetAllPoints(Addon.c)

Addon.l.continent = "Kalimdor"
Addon.c.continent = "Eastern Kingdoms"
Addon.r.continent = "Outland"

Addon.sidesHeight = 400 * UIParent:GetEffectiveScale()
Addon.sidesWidth = 10 * UIParent:GetEffectiveScale()
Addon.sidesTop = ((Addon.size * ZOOM) - (Addon.size * ZOOM * 0.88)) / 2 -- Source texture is 88% of file height

Addon.l.left = Addon:CreateTexture()
Addon.l.left:SetTexture("Interface/ACHIEVEMENTFRAME/UI-Achievement-MetalBorder-Left")
--C:\Program Files (x86)\World of Warcraft\_classic_\BlizzardInterfaceArt\Interface\COMMON\UI-Goldborder-!tile.png
--C:\Program Files (x86)\World of Warcraft\_classic_\BlizzardInterfaceArt\Interface\COMMON\UI-Goldborder-_tile.png
--C:\Program Files (x86)\World of Warcraft\_classic_\BlizzardInterfaceArt\Interface\COMMON\UI-Goldborder.png
Addon.l.left:SetPoint("TOPLEFT", Addon.l, "TOPLEFT", 0, -Addon.sidesTop)
Addon.l.left:SetWidth(Addon.sidesWidth * ZOOM)
Addon.l.left:SetHeight(Addon.sidesHeight * ZOOM)--453
Addon.l.left:SetDrawLayer(Addon.l.left:GetDrawLayer(), 0)

Addon.l.title = Addon.l:CreateFontString(Addon:GetName().."LeftTitle","OVERLAY","QuestTitleFont")
Addon.l.title:SetPoint("BOTTOM", Addon.l, "TOP", 0, -16)
Addon.l.title:SetText(Addon.l.continent)

Addon.r.title = Addon.l:CreateFontString(Addon:GetName().."RightTitle","OVERLAY","QuestTitleFont")
Addon.r.title:SetPoint("BOTTOM", Addon.r, "TOP", 0, -16)
Addon.r.title:SetText(Addon.r.continent)

Addon.c.title = Addon.l:CreateFontString(Addon:GetName().."CenterTitle","OVERLAY","QuestTitleFont")
Addon.c.title:SetPoint("BOTTOM", Addon.c, "TOP", 0, -16)
Addon.c.title:SetText(Addon.c.continent)

Addon.cities = {}
Addon.cities["Eastern Kingdoms"] = {
  {["faction"]="a", ["name"]="Ironforge",   ["x"]=0.43, ["y"]=0.58, ["icon"]="Capital", ["nameY"]= 20},
  {["faction"]="h", ["name"]="Silvermoon",  ["x"]=0.57, ["y"]=0.14, ["icon"]="Capital", ["nameY"]= 20},
  {["faction"]="h", ["name"]="Stonard",     ["x"]=0.51, ["y"]=0.76, ["icon"]="Town",    ["nameY"]= 20},
  {["faction"]="a", ["name"]="Stormwind",   ["x"]=0.38, ["y"]=0.71, ["icon"]="Capital", ["nameY"]=-20},
  {["faction"]="h", ["name"]="Undercity",   ["x"]=0.38, ["y"]=0.33, ["icon"]="Capital", ["nameY"]= 20},
  -- {["faction"]="n", ["name"]="Karazhan",   ["x"]=0.45, ["y"]=0.79, ["icon"]="Town"},
}
Addon.cities["Kalimdor"] = {
  {["faction"]="a", ["name"]="Darnassus",     ["x"]=0.33, ["y"]=0.08, ["icon"]="Capital", ["nameY"]= 20},
  {["faction"]="a", ["name"]="Exodar",        ["x"]=0.18, ["y"]=0.23, ["icon"]="Capital", ["nameY"]=-20},
  {["faction"]="a", ["name"]="Theramore",     ["x"]=0.60, ["y"]=0.63, ["icon"]="Town",    ["nameY"]= 20},
  {["faction"]="h", ["name"]="Thunder Bluff", ["x"]=0.40, ["y"]=0.53, ["icon"]="Capital", ["nameY"]= 20},
  {["faction"]="h", ["name"]="Orgrimmar",     ["x"]=0.59, ["y"]=0.44, ["icon"]="Capital", ["nameY"]= 20},
}

Addon.cities["Outland"] = {
  {["faction"]="n", ["name"] = "Shattrath", ["x"]=0.38, ["y"]=0.64, ["icon"]="Capital"}
}

local faction = UnitFactionGroup("player")
if faction == "Alliance" then
  faction = "a"
else
  faction = "h"
end

local macro1 =
"/stopcasting" .. CRLF ..
"/stand" .. CRLF ..
"/dismount" .. CRLF ..
"/cast "

local macro2 =
CRLF ..
"/run MagePort:ChangeState(" .. Addon.FADE_OUT .. ")"

Addon.secure = CreateFrame("Button", "Secure", Addon, "SecureHandlerClickTemplate, SecureHandlerShowHideTemplate", "SecureHandlerMouseUpDownTemplate")
Addon.secure:SetFrameRef("test", Addon)
Addon.secure:SetAttribute(
  "_onclick",
  [[
    local Addon = self:GetFrameRef("test")
    Addon:CallMethod("ChangeState", nil, true)
    Addon:Show()
  ]]
)
Addon.secure:SetAttribute(
  "_onmouseup",
  [[
    print(self, button)
  ]]
)
Addon.secure:SetAttribute(
  "_onmousedown",
  [[
    print(self, button)
  ]]
)
--------------------------------------------------------------------------------

-- Handle state changes
function Addon:ChangeState(newState, fromSecure)
  if fromSecure then
    if Addon.state == Addon.INVISIBLE or Addon.state == Addon.FADE_OUT then
      newState = Addon.FADE_IN
    else
      newState = Addon.FADE_OUT
    end
  end

  if Addon.state == Addon.INVISIBLE then
    if newState == Addon.FADE_IN then
      -- Go from invisible to fading in
      Addon.state = newState
      Addon.alpha = 0
      Addon.start = GetTime()
      Addon.stop  = Addon.start + Addon.speed
    end
  elseif Addon.state == Addon.FADE_IN then
    if newState == Addon.VISIBLE then
      -- Now fully visible, after fading in
      Addon.state = newState
      Addon.alpha = 1
    elseif newState == Addon.FADE_OUT then
      -- User let go of key early, fadeout
      Addon.state = newState
      Addon.start = GetTime() - (1 - Addon.alpha) * Addon.speed
      Addon.stop  = Addon.start + Addon.speed
    end
  elseif Addon.state == Addon.VISIBLE then
    if newState == Addon.FADE_OUT then
      -- User let go of key, fadeout
      Addon.state = newState
      Addon.start = GetTime()
      Addon.stop  = Addon.start + Addon.speed
    end
  elseif Addon.state == Addon.FADE_OUT then
    if newState == Addon.FADE_IN then
      -- User pressed key while fading out, fade back in
      Addon.state = newState
      Addon.start = GetTime() - Addon.alpha * Addon.speed
      Addon.stop  = Addon.start + Addon.speed
    elseif newState == Addon.INVISIBLE then
      -- Now fully invisible after fading out
      Addon.state = newState
      Addon.alpha = 0
      Addon:Hide()
    end
  end
end

--------------------------------------------------------------------------------

function Addon:OnEvent(event, ...)
  if event == "ADDON_LOADED" then
    local name = select(1, ...)
    if name ~= Addon:GetName() then
      return
    end
    
    -- Addon initial state
    if not savesChar then
      savesChar = {
        size   = Addon.size,
        toggle = Addon.toggle
      }
    end
    Addon:Init()
  elseif event == "PLAYER_ENTERING_WORLD" then
    for i,frame in ipairs(Addon.frames) do
      local continent = frame.continent
      local cities = Addon.cities[continent]
      for j,city in pairs(cities) do
        if city.faction == faction or city.faction == "n" then
          local usable, nomana = IsUsableSpell("Teleport: " .. city.name)
          if usable or usable and nomana then
            local n = frame:GetName().."City"..j
            if not _G[n] then
              local f = CreateFrame("Frame", n, frame)
              frame[city.name.."f"] = f
              f.city = city
              f.t = Addon:CreateTexture()
              f.t:SetAtlas("poi-majorcity", false)
              if city.icon == "Town" then
                f.t:SetAtlas("poi-town", false)
              end
              f.t:SetDrawLayer("ARTWORK", 1)
              f.t:SetAllPoints(f)
      
              f.tele = CreateFrame("Button", f:GetName().."Tele", f, "SecureActionButtonTemplate")
              f.tele:SetAttribute("type","macro") -- "type" means any click including modifiers
              f.tele:SetAttribute("macrotext", macro1 .. "Teleport: " ..city.name .. macro2)
              f.tele:SetNormalTexture("Interface/FriendsFrame/UI-Toast-FriendOnlineIcon")
              f.tele:SetPoint("RIGHT", f, "LEFT")
              f.tele:RegisterForClicks("LeftButtonUp")
      
              f.port = CreateFrame("Button", f:GetName().."Port", f, "SecureActionButtonTemplate")
              f.port:SetAttribute("type","macro")
              f.port:SetAttribute("macrotext", macro1 .. "Portal: " ..city.name .. macro2)
              f.port:SetNormalTexture("Interface/FriendsFrame/UI-Toast-ChatInviteIcon")
              f.port:SetPoint("LEFT", f, "RIGHT")
              f.port:RegisterForClicks("LeftButtonUp")

              f.title =
              f:CreateFontString(
                Addon:GetName()..city.name.."Title",
                "OVERLAY",
                "QuestTitleFont"
              )
              f.title:SetPoint("TOP", f, "BOTTOM")
              f.title:SetText(city.name)
              -- f.title:SetTextColor(1.0, 1.0, 1.0, 1.0)
            end
          end
        end
      end
    end
  end
end

-- If ESC pressed to close window, don't immediately hide
function Addon:OnHide()
  if Addon.state ~= Addon.INVISIBLE then
    Addon:Show()
    Addon:ChangeState(Addon.FADE_OUT)
  end
end

function Addon:OnUpdate(...)
  local now = GetTime()
  if Addon.state == Addon.VISIBLE then
    return
  elseif Addon.state == Addon.FADE_IN then
    if now >= Addon.stop then
      Addon:ChangeState(Addon.VISIBLE)
      now = Addon.stop
    end
    local percent = (now - Addon.start) / Addon.speed
    Addon.alpha = percent
  elseif Addon.state == Addon.FADE_OUT then
    if now >= Addon.stop then
      Addon:ChangeState(Addon.INVISIBLE)
      now = Addon.stop
    end
    local percent = (Addon.stop - now) / Addon.speed
    Addon.alpha = percent
  end

  Addon:SetAlpha(Addon.alpha)
  -- Scale the size of the aperature based on the current alpha value
  -- Lower the alpha, the larger the aperature. Higher, the smaller.
  local zoom = ZOOM - 1
  local size = Addon.size * (1+zoom - (zoom*Addon.alpha))
  Addon.l:SetSize(size, size)
  Addon.r:SetSize(size, size)
  Addon.c:SetSize(size, size)

  local sizeSidesWidth = Addon.sidesWidth * (1+zoom - (zoom*Addon.alpha))
  local sizeSidesHeight = Addon.sidesHeight * (1+zoom - (zoom*Addon.alpha))
  local sidesTop = Addon.sidesTop * (1+zoom - (zoom*Addon.alpha))
  Addon.l.left:SetPoint("TOPLEFT", Addon.l, "TOPLEFT", 0, -sidesTop)
  Addon.l.left:SetSize(sizeSidesWidth, sizeSidesHeight)

  for i,frame in ipairs(Addon.frames) do
    local continent = frame.continent
    local cities = Addon.cities[continent]
    for j,city in pairs(cities) do
      if city.faction == faction or city.faction == "n" then
        local f = frame[city.name.."f"]
        if f then
          local iconSize = 24*UIParent:GetEffectiveScale() * (1+zoom - (zoom*Addon.alpha))
          f:SetAlpha(Addon.alpha)
          f:SetSize(iconSize, iconSize)
          f:SetPoint(
            "TOPLEFT",
            frame,
            "TOPLEFT",
            city.x*size,
            -city.y*size
          )
          f.tele:SetAlpha(Addon.alpha)
          f.port:SetAlpha(Addon.alpha)
          f.tele:SetSize(iconSize, iconSize)
          f.port:SetSize(iconSize, iconSize)
        end
      end
    end
  end
end

Addon:SetScript("OnEvent", Addon.OnEvent)
Addon:SetScript("OnHide", Addon.OnHide)
Addon:SetScript("OnUpdate", Addon.OnUpdate)
Addon:RegisterEvent("ADDON_LOADED")
Addon:RegisterEvent("PLAYER_ENTERING_WORLD")

--------------------------------------------------------------------------------

-- Interface Options helper function
function Addon.CreateSliderAndEditBox(name, parent, sibling, xoff, yoff)
  local slider =
  CreateFrame(
    "Slider",
    parent:GetName() .. name,
    parent,
    "OptionsSliderTemplate"
  )
  slider:SetPoint(
    "TOPLEFT", sibling:GetName() or parent:GetName(), "BOTTOMLEFT", xoff, yoff
  )
  slider:SetWidth(500)
  slider:SetHeight(15)
  slider:SetOrientation("HORIZONTAL")
  slider.Text:SetTextColor(1, 0.82, 0)
  local function OnValueChanged(self, value)
    -- Set the edit box to match the slider value
    if slider:GetValueStep() >= 1 then
      -- Use whole numbers only, remove all decimals
      value = value - math.fmod(value, self:GetValueStep())
    end
    self.box:SetText(value)
  end
  slider:SetScript("OnValueChanged", OnValueChanged)

  slider.box = CreateFrame(
    "EditBox", slider:GetName() .. "Box", slider, "InputBoxTemplate"
  )
  slider.box:SetPoint("TOP", slider:GetName(), "BOTTOM", 0, -15)
  slider.box:SetAutoFocus(false)
  slider.box:SetFontObject(GameFontHighlightSmall)
  slider.box:SetHeight(14)
  slider.box:SetWidth(70)
  slider.box:SetJustifyH("CENTER")
  slider.box:EnableMouse(true)
  slider.box:SetMaxLetters(4)
  
  local function FocusLost(self)
    self:ClearFocus()
    self:ClearHighlightText()
    C_Timer.After(
      0.05,
      function()
        -- Restore field value, as enter/tab wasn't used
        local value = slider:GetValue()
        OnValueChanged(slider, value)
      end
    )
  end
  slider.box:SetScript("OnEditFocusLost", FocusLost)

  slider.box:SetScript(
    "OnEscapePressed",
    function(self)
      self:ClearFocus()
      self:SetText(tostring(slider:GetValue()))
    end
  )
  
  local function EnterTab(self)
    self:ClearFocus()
    self:ClearHighlightText()
    -- Ensure only a valid number was entered
    local value = tonumber(self:GetText()) or slider:GetValue()
    slider:SetValue(value)
  end
  
  slider.box:SetScript("OnEnterPressed", EnterTab)
  slider.box:SetScript("OnTabPressed", EnterTab)
  return slider
end

-- Interface options panel
Addon.panel = CreateFrame("Frame", Addon:GetName() .. "Panel", UIParent)
Addon.panel.name = Addon:GetName()
Addon.panel:Hide() -- Needed or child objects won't get OnShow first view
Addon.panel.title =
Addon.panel:CreateFontString(
  Addon.panel:GetName() .. "Title", "OVERLAY", "GameFontNormalLarge"
)
Addon.panel.title:SetPoint("TOPLEFT", Addon.panel:GetName(), "TOPLEFT", 15, -15)
Addon.panel.title:SetText(
  Addon:GetName() .. " " .. GetAddOnMetadata("MagePort", "Version")
)

-- Toggle
Addon.panel.toggle =
CreateFrame(
  "CheckButton",
  Addon:GetName() .. "Toggle",
  Addon.panel,
  "ChatConfigCheckButtonTemplate"
)
Addon.panel.toggle:SetPoint(
  "TOPLEFT", Addon.panel.title:GetName(), "BOTTOMLEFT", 0, -15
)
Addon.panel.toggle.Text:SetText(" Toggle instead of hold")

-- Aperature size
Addon.panel.size =
Addon.CreateSliderAndEditBox(
  Addon.panel:GetName() .. "Size", Addon.panel, Addon.panel.toggle, 0, -65
)
Addon.panel.size:SetMinMaxValues(128, 1024)
Addon.panel.size:SetValue(Addon.size)
Addon.panel.size:SetValueStep(1)
Addon.panel.size.Low:SetText("128px")
Addon.panel.size.High:SetText("1024px")
Addon.panel.size.Text:SetText("Aperature Size")
Addon.panel.size.tooltipText = "How much screen to hide when showing the mouse."
Addon.panel.size.box:SetText(Addon.size)

-- Panel OK and Cancel
Addon.panel.okay =
function(self)
  -- Save changed settings
  Addon.size   = Addon.panel.size:GetValue()
  Addon.toggle = Addon.panel.toggle:GetChecked()
  
  savesChar.size   = Addon.size
  savesChar.toggle = Addon.toggle
end

Addon.panel.cancel =
function(self)
  -- Restore all settings
  Addon.panel.size:SetValue(Addon.size)
  Addon.panel.toggle:SetChecked(Addon.toggle)
end

-- Add to interface options
InterfaceOptions_AddCategory(Addon.panel)

--------------------------------------------------------------------------------

-- /console scriptErrors 1
-- /console scriptErrors 0
-- DevTools_Dump
